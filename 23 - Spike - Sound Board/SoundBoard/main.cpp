#include <iostream>
#include <SDL.h>
#include <SDL_mixer.h>
#include <string>
#include <stdio.h>

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

int main(int argc, char * argv[])
{
	SDL_Window* window = NULL;
	SDL_Surface* screenSurface = NULL;
	Mix_Music *music = NULL;
	SDL_Event e;
	Mix_Chunk *baby = NULL;
	Mix_Chunk *bicycle = NULL;
	Mix_Chunk *applause = NULL;
	bool quit = false;

	SDL_Init(SDL_INIT_EVERYTHING);
	Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048);
	baby = Mix_LoadWAV("baby_x.wav");
	applause = Mix_LoadWAV("applause_y.wav");
	bicycle = Mix_LoadWAV("bicycle_bell.wav");
	music = Mix_LoadMUS("beat.wav");
	window = SDL_CreateWindow("Sound Board", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	screenSurface = SDL_GetWindowSurface(window);
	SDL_FillRect(screenSurface, NULL, SDL_MapRGB(screenSurface->format, 0xFF, 0xFF, 0xFF));

	while (!quit)
	{
		while (SDL_PollEvent(&e))
		{
			if (e.type == SDL_QUIT)
			{
				quit = true;
			}
			if (e.type == SDL_KEYDOWN)
			{
				switch (e.key.keysym.sym)
				{
				case SDLK_1:
					Mix_PlayChannel(-1, baby, 0);
					break;
				case SDLK_2:
					Mix_PlayChannel(-1, applause, 0);
					break;
				case SDLK_3:
					Mix_PlayChannel(-1, bicycle, 0);
					break;
				case SDLK_0:
					if (Mix_PlayingMusic() == 0)
					{
						Mix_PlayMusic(music, -1);
					}
					else
					{
						if(Mix_PausedMusic() == 1)
						{
							Mix_ResumeMusic();
						}
						else
						{
							Mix_PauseMusic();
						}
					}
				}
			}
		}
		SDL_UpdateWindowSurface(window);
	}
	return 0;
}