#include "testClass.h"

testClass::testClass()
{
	privateVariable = 5;
	publicVariable = 10;
}

int testClass::product()
{
	return privateVariable * publicVariable;
}