#include <iostream>
#include <cstdlib>
#include <string>
#include <sstream>
#include "testClass.h"

using namespace std;

void stepOne(int value1, int value2)
{
	cout << value1 << " " << value2 << endl;
}

int stepTwo(int value1)
{
	return value1 * 10;
}

void stepFour()
{
	for(int i=1;i<20;i=i+2)
	{
		cout << i << " ";
	}
	cout << endl;
}

void stepFive()
{
	int array1 [5];
	for (int i = 0; i < 5; i++)
	{
		array1[i] = rand() % 100;
		cout << array1[i] << " ";
	}
	cout << endl;
}

void main()
{
	stepOne(1, 2); //Step 1

	cout << stepTwo(1) << endl; //Step 2

	int pointerTest = 1; //Step 3
	int *pointer1 = &pointerTest;
	cout << pointerTest << endl;
	cout << *pointer1 << endl;
	*pointer1 = 5;
	cout << pointerTest << endl;
	cout << *pointer1 << endl;

	stepFour(); //Step 4

	stepFive(); //Step 5

	string test = "this has spaces in it"; //Step 6
	istringstream iss(test);
	while (iss)
	{
		string test1;
		iss >> test1;
		cout << test1 << endl;
	}

	testClass testClass1; //Step 7
	cout << testClass1.product() << endl;
}