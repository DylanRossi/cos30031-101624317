#include <iostream>
#include <thread>
#include <windows.h>

using namespace std;

void getMoves(int position[2], char map[9][8], char *acceptableMoves)
{
	cout << "You can move ";
	if (map[position[0] - 1][position[1]] != '#')
	{
		acceptableMoves[0] = 'N';
		cout << "N,";
	}
	if (map[position[0] + 1][position[1]] != '#')
	{
		acceptableMoves[1] = 'S';
		cout << "S,";
	}
	if (map[position[0]][position[1] + 1] != '#')
	{
		acceptableMoves[2] = 'E';
		cout << "E,";
	}
	if (map[position[0]][position[1] - 1] != '#')
	{
		acceptableMoves[3] = 'W';
		cout << "W";
	}
	cout << ":> " << endl;
}

void update(char answer, char map[9][8], int *position)
{
	switch (answer)
	{
	case 'N':
		position[0]--;
		break;
	case 'S':
		position[0]++;
		break;
	case 'E':
		position[1]++;
		break;
	case 'W':
		position[1]--;
		break;
	default:
		break;
	}
}

void render(int position[2], bool *gameEnd, char map[9][8])
{
	if (map[position[0]][position[1]] == 'D')
	{
		cout << "Arrrrgh... you've fallen down a pit." << endl;
		cout << "YOU HAVE DIED!" << endl;
		cout << "Thanks for playing. Maybe next time." << endl;
		*gameEnd = true;
	}
	if (map[position[0]][position[1]] == 'G')
	{
		cout << "Wow - you've discovered a large chest filled with GOLD coins!" << endl;
		cout << "YOU WIN!" << endl;
		cout << "Thanks for playing. There probably won't be a next time." << endl;
		*gameEnd = true;
	}
}

bool checkMoves(char answer, char acceptableMoves[4])
{
	for (int i = 0; i < 4; i++)
	{
		if (answer == acceptableMoves[i])
		{
			return true;
		}
	}
	return false;
}

void processInput(char answer, bool *gameEnd, char map[9][8], int *position, char acceptableMoves[4])
{
	answer = toupper(answer);
	if (answer == 'Q')
	{
		*gameEnd = true;
	}
	else if (checkMoves(answer, acceptableMoves))
	{
		update(answer, map, position);
		render(position, gameEnd, map);
	}
	else
	{
		cout << "That's not a valid command!" << endl;
	}
}

void gameLoop(bool *gameEnd)
{
	int timer = 0;
	while (!*gameEnd)
	{
		cout << '\t' << timer << '\r' << flush;
		timer++;
		Sleep(1000);
	}
}

void detectInput(bool *gameEnd)
{
	char map[9][8] =
	{
		{ '#', '#', '#', '#', '#', '#', '#', '#' },
		{ '#', 'G', ' ', 'D', '#', 'D', ' ', '#' },
		{ '#', ' ', ' ', ' ', '#', ' ', ' ', '#' },
		{ '#', '#', '#', ' ', '#', ' ', 'D', '#' },
		{ '#', ' ', ' ', ' ', '#', ' ', ' ', '#' },
		{ '#', ' ', '#', '#', '#', '#', ' ', '#' },
		{ '#', ' ', ' ', ' ', ' ', ' ', ' ', '#' },
		{ '#', '#', 'S', '#', '#', '#', '#', '#' },
		{ '#', '#', '#', '#', '#', '#', '#', '#' }
	};
	int position[2] = { 7, 2 };
	char answer;
	while (!*gameEnd)
	{
		char acceptableMoves[4];
		getMoves(position, map, acceptableMoves);
		cin >> answer;
		processInput(answer, gameEnd, map, position, acceptableMoves);
	}
}

int main()
{
	bool gameEnd = false;
	bool *gameEndPointer = &gameEnd;
	
	cout << "Welcome to GridWorld: Quantised Excitement. Fate is waiting for You!" << endl;
	cout << "Valid commands: N, S, E and W for direction. Q to quit the game." << endl;

	thread gameLoopThread(gameLoop, gameEndPointer);
	thread detectInputThread(detectInput, gameEndPointer);

	gameLoopThread.join();
	detectInputThread.join();
	
	return 0;
}