#include <iostream>
#include <string>
#include "SelectAdventure.h"

using namespace std;

SelectAdventure::SelectAdventure() : Stage()
{

}

SelectAdventure::~SelectAdventure()
{

}

void SelectAdventure::Handle(StageManager* pStageManager)
{
	char answer;
	cout << "Zorkish :: Select Adventure" << endl;
	cout << "--------------------------------------------------------" << endl;
	cout << endl;
	cout << "Choose your adventure:" << endl;
	cout << endl;
	cout << "1. Test World" << endl;
	cout << endl;
	cout << "Select a world:>";
	cin >> answer;
	pStageManager->ChangeStage(StageManager::sGamePlay);
	pStageManager->Handle();
}