#include <iostream>
#include <string>
#include "TestWorld.h"

using namespace std;

TestWorld::TestWorld() : Stage()
{

}

TestWorld::~TestWorld()
{

}

void TestWorld::Handle(StageManager* pStageManager)
{
	string answer;
	cout << "Welcome to Zorkish: Test World" << endl;
	cout << "This world is simple and pointless. Used it to test Zorksih phase 1 spec." << endl;
	cout << ":> ";
	cin >> answer;
	if (answer == "hiscore")
	{
		cout << "You have entered the magic word and will now see the New High Score screen." << endl;
		pStageManager->ChangeStage(StageManager::sHighScore);
		pStageManager->Handle();
	}
	else
	{
		cout << "Your adventure has ended without fame or fortune." << endl;
		pStageManager->ChangeStage(StageManager::sMainMenu);
		pStageManager->Handle();
	}
}