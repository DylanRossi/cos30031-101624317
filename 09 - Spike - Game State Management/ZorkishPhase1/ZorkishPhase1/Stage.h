#pragma once

#include <iostream>
#include <string>

class StageManager;

using namespace std;

class Stage
{
public:
	Stage();
	virtual ~Stage();
	virtual void Handle(StageManager* pStageManager);
};