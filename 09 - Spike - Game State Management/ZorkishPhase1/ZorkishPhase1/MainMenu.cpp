#include <iostream>
#include <string>
#include "MainMenu.h"

using namespace std;

MainMenu::MainMenu() : Stage()
{

}

MainMenu::~MainMenu()
{

}

void MainMenu::Handle(StageManager* pStageManager)
{
	char answer;
	cout << "Zorkish :: Main Menu" << endl;
	cout << "--------------------------------------------------------" << endl;
	cout << endl;
	cout << "Welcome to Zorkish Adventues" << endl;
	cout << endl;
	cout << "1. Select Adventure and Play" << endl;
	cout << "2. Hall of Fame" << endl;
	cout << "3. Help" << endl;
	cout << "4. About" << endl;
	cout << "5. Quit" << endl;
	cout << endl;
	cout << "Select 1-5:> ";
	cin >> answer;
	switch (answer)
	{
	case '1':
		pStageManager->ChangeStage(StageManager::sSelectAdventure);
		pStageManager->Handle();
		break;
	case '2':
		pStageManager->ChangeStage(StageManager::sHOF);
		pStageManager->Handle();
		break;
	case '3':
		pStageManager->ChangeStage(StageManager::sHelp);
		pStageManager->Handle();
		break;
	case '4':
		pStageManager->ChangeStage(StageManager::sAbout);
		pStageManager->Handle();
		break;
	case '5':
		break;
	default:
		cout << "You broke something" << endl;
		break;
	}
}