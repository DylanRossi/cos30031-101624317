#pragma once

#include <iostream>
#include <string>
#include "Stage.h"
#include "StageManager.h"

using namespace std;

class SelectAdventure : public Stage
{
public:
	SelectAdventure();
	virtual ~SelectAdventure();
	virtual void Handle(StageManager* pStageManager);
};