#pragma once

#include <iostream>
#include <string>
#include "Stage.h"
#include "StageManager.h"

using namespace std;

class About : public Stage
{
public:
	About();
	virtual ~About();
	virtual void Handle(StageManager* pStageManager);
};