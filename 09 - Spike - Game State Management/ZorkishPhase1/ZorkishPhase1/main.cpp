#include <iostream>
#include <string>
#include "StageManager.h"
#include "Stage.h"
#include "MainMenu.h"


using namespace std;

int main()
{
	StageManager pStageManager;
	pStageManager.Handle();
	return 0;
}