#pragma once

#include <iostream>
#include <string>
#include "MainMenu.h"
#include "About.h"
#include "Help.h"
#include "SelectAdventure.h"
#include "HighScore.h"
#include "TestWorld.h"
#include "HOF.h"

class Stage;

using namespace std;

class StageManager
{
private:
	Stage* fStage;
public:
	enum Stages
	{
		sMainMenu,
		sHelp,
		sAbout,
		sHighScore,
		sSelectAdventure,
		sHOF,
		sQuit,
		sGamePlay
	};

	StageManager();
	~StageManager();
	void Handle();
	void ChangeStage(Stages pStage);
};