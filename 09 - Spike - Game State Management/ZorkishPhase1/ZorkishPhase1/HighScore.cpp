#include <iostream>
#include <string>
#include "HighScore.h"

using namespace std;

HighScore::HighScore() : Stage()
{

}

HighScore::~HighScore()
{

}

void HighScore::Handle(StageManager* pStageManager)
{
	string name;
	cout << "Zorkish :: New High Score" << endl;
	cout << "--------------------------------------------------------" << endl;
	cout << endl;
	cout << "Congratulations!" << endl;
	cout << endl;
	cout << "You have made it to the Zorkish Hall of Fame" << endl;
	cout << endl;
	cout << "Adventure: Test World" << endl;
	cout << "Score: -1" << endl;
	cout << "Moves: 1" << endl;
	cout << endl;
	cout << "Please type your name and press enter:" << endl;
	cout << ":> ";
	cin >> name;
	pStageManager->ChangeStage(StageManager::sMainMenu);
	pStageManager->Handle();
}