#include <iostream>
#include <string>
#include "Help.h"

using namespace std;

Help::Help() : Stage()
{

}

Help::~Help()
{

}

void Help::Handle(StageManager* pStageManager)
{
	cout << "Zorkish :: Help" << endl;
	cout << "--------------------------------------------------------" << endl;
	cout << endl;
	cout << "The following commands are supported:" << endl;
	cout << "quit," << endl;
	cout << "hitscore (for testing)" << endl;
	cout << endl;
	cout << "Press Enter to return to the Main Menu" << endl;
	cin.get();
	if (cin.get())
	{
		pStageManager->ChangeStage(StageManager::sMainMenu);
		pStageManager->Handle();
	}
}