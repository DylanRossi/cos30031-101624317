#pragma once

#include <iostream>
#include <string>
#include "Stage.h"
#include "StageManager.h"

using namespace std;

class Help : public Stage
{
public:
	Help();
	virtual ~Help();
	virtual void Handle(StageManager* pStageManager);
};