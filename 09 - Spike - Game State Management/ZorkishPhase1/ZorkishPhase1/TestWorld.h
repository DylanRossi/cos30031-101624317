#pragma once

#include <iostream>
#include <string>
#include "Stage.h"
#include "StageManager.h"

using namespace std;

class TestWorld : public Stage
{
public:
	TestWorld();
	virtual ~TestWorld();
	virtual void Handle(StageManager* pStageManager);
};