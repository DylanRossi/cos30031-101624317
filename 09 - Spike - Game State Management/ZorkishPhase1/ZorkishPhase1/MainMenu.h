#pragma once

#include <iostream>
#include <string>
#include "Stage.h"
#include "StageManager.h"

class StageManager;

using namespace std;

class MainMenu : public Stage
{
public:
	MainMenu();
	virtual ~MainMenu();
	virtual void Handle(StageManager* pStageManager);
};