#include <iostream>
#include <string>
#include "StageManager.h"

using namespace std;

StageManager::StageManager() : fStage(new MainMenu())
{
}

StageManager::~StageManager()
{
	delete fStage;
}

void StageManager::Handle()
{
	fStage->Handle(this);
}

void StageManager::ChangeStage(Stages pStage)
{
	delete fStage;
	if (pStage == sMainMenu)
	{
		fStage = new MainMenu();
	}
	if (pStage == sAbout)
	{
		fStage = new About();
	}
	if (pStage == sHelp)
	{
		fStage = new Help();
	}
	if (pStage == sSelectAdventure)
	{
		fStage = new SelectAdventure();
	}
	if (pStage == sHighScore)
	{
		fStage = new HighScore();
	}
	if (pStage == sGamePlay)
	{
		fStage = new TestWorld();
	}
	if (pStage == sHOF)
	{
		fStage = new HOF();
	}
	cout << endl;
}