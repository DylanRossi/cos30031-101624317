#include <iostream>
#include <string>
#include <SDL.h>

using namespace std;

class Square
{
private:
	SDL_Rect collider;
	int color1, color2, color3, velX, velY;
public:
	static const int SCREEN_WIDTH = 700;
	static const int SCREEN_HEIGHT = 500;
	static const int SQUARE_WIDTH = 100;
	static const int SQUARE_HEIGHT = 100;
	static const int VELOCITY = 10;

	Square();

	bool CheckCollision(SDL_Rect b);
	void HandleEvent(SDL_Event& e);
	void Move(SDL_Rect& object2);
	void Render(SDL_Renderer* renderer);
};
