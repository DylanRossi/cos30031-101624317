#include <iostream>
#include <string>
#include <SDL.h>
#include "Circle.h"

using namespace std;

Circle::Circle(int x, int y, int width, int height, SDL_Texture* textureA, SDL_Texture* textureB)
{
	posX = x;
	posY = y;
	cWidth = width;
	cHeight = height;
	collider.r = cWidth / 2;
	velX = 0;
	velY = 0;
	cTextureA = textureA;
	cTextureB = textureB;
	cTexture = cTextureA;

	ShiftColliders();
}

void Circle::Move(CircleStruct& circle)
{
	posX += velX;
	ShiftColliders();

	if ((posX < 0) || (posX + collider.r > SCREEN_WIDTH))
	{
		posX -= velX;
		ShiftColliders();
	}

	posY += velY;
	ShiftColliders();

	if ((posY < 0) || (posY + collider.r> SCREEN_HEIGHT))
	{
		posY -= velY;
		ShiftColliders();
	}

	if (CheckCollision(circle))
	{
		cTexture = cTextureB;
	}
	else
	{
		cTexture = cTextureA;
	}
}

void Circle::Render(SDL_Renderer* renderer)
{
	SDL_Rect renderQuad = { posX-collider.r, posY-collider.r, cWidth, cHeight };

	SDL_RenderCopy(renderer, cTexture, NULL, &renderQuad);
}

bool Circle::CheckCollision(CircleStruct& b)
{
	int totalRadiusSquared = collider.r + b.r;
	totalRadiusSquared = totalRadiusSquared * totalRadiusSquared;

	if (DistanceSquared(b.x, b.y) < (totalRadiusSquared))
	{
		return true;
	}
	return false;
}

double Circle::DistanceSquared(int x2, int y2)
{
	int deltaX = x2 - collider.x;
	int deltaY = y2 - collider.y;
	return deltaX * deltaX + deltaY * deltaY;
}

CircleStruct& Circle::GetCollider()
{
	return collider;
}

void Circle::HandleEvent(SDL_Event& e)
{
	if (e.type == SDL_KEYDOWN && e.key.repeat == 0)
	{
		switch (e.key.keysym.sym)
		{
		case SDLK_UP: velY -= CIRCLE_VELOCITY; break;
		case SDLK_DOWN: velY += CIRCLE_VELOCITY; break;
		case SDLK_LEFT: velX -= CIRCLE_VELOCITY; break;
		case SDLK_RIGHT: velX += CIRCLE_VELOCITY; break;
		}
	}
	else if (e.type == SDL_KEYUP && e.key.repeat == 0)
	{
		switch (e.key.keysym.sym)
		{
		case SDLK_UP: velY += CIRCLE_VELOCITY; break;
		case SDLK_DOWN: velY -= CIRCLE_VELOCITY; break;
		case SDLK_LEFT: velX += CIRCLE_VELOCITY; break;
		case SDLK_RIGHT: velX -= CIRCLE_VELOCITY; break;
		}
	}
}

void Circle::ShiftColliders()
{
	collider.x = posX;
	collider.y = posY;
}