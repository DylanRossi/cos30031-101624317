#include <iostream>
#include <string>
#include <SDL.h>
#include "Square.h"

using namespace std;

Square::Square()
{
	collider.x = 0;
	collider.y = 0;

	collider.w = SQUARE_WIDTH;
	collider.h = SQUARE_HEIGHT;

	velX = 0;
	velY = 0;
}


void Square::Render(SDL_Renderer* renderer)
{
	SDL_SetRenderDrawColor(renderer, color1, color2, color3, 0x00);
	SDL_RenderDrawRect(renderer, &collider);
	SDL_RenderFillRect(renderer, &collider);
}


bool Square::CheckCollision(SDL_Rect b)
{
	int leftA, leftB;
	int rightA, rightB;
	int topA, topB;
	int bottomA, bottomB;

	leftA = collider.x;
	rightA = collider.x + collider.w;
	topA = collider.y;
	bottomA = collider.y + collider.h;

	leftB = b.x;
	rightB = b.x + b.w;
	topB = b.y;
	bottomB = b.y + b.h;

	if (bottomA <= topB)
	{
		return false;
	}

	if (topA >= bottomB)
	{
		return false;
	}

	if (rightA <= leftB)
	{
		return false;
	}

	if (leftA >= rightB)
	{
		return false;
	}

	return true;
}

void Square::Move(SDL_Rect& square2)
{
	collider.x += velX;

	if ((collider.x < 0) || (collider.x + SQUARE_WIDTH > SCREEN_WIDTH))
	{
		collider.x -= velX;
	}

	collider.y += velY;

	if ((collider.y < 0) || (collider.y + SQUARE_HEIGHT > SCREEN_HEIGHT))
	{
		collider.y -= velY;
	}

	if (CheckCollision(square2))
	{
		color1 = 0x00;
		color2 = 0xFF;
		color3 = 0xFF;
	}
	else
	{
		color1 = 0xFF;
		color2 = 0x00;
		color3 = 0x00;
	}
}

void Square::HandleEvent(SDL_Event& e)
{
	if (e.type == SDL_KEYDOWN && e.key.repeat == 0)
	{
		switch (e.key.keysym.sym)
		{
		case SDLK_UP: velY -= VELOCITY; break;
		case SDLK_DOWN: velY += VELOCITY; break;
		case SDLK_LEFT: velX -= VELOCITY; break;
		case SDLK_RIGHT: velX += VELOCITY; break;
		}
	}
	else if (e.type == SDL_KEYUP && e.key.repeat == 0)
	{
		switch (e.key.keysym.sym)
		{
		case SDLK_UP: velY += VELOCITY; break;
		case SDLK_DOWN: velY -= VELOCITY; break;
		case SDLK_LEFT: velX += VELOCITY; break;
		case SDLK_RIGHT: velX -= VELOCITY; break;
		}
	}
}


