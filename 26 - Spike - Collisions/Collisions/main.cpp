#include <SDL.h>
#include <iostream>
#include <string>
#include "Square.h"
#include "Circle.h"

using namespace std;

const int SCREEN_WIDTH = 700;
const int SCREEN_HEIGHT = 500;

void init();



SDL_Window* window = NULL;

SDL_Renderer* renderer = NULL;

void init()
{
	SDL_Init(SDL_INIT_EVERYTHING);
	window = SDL_CreateWindow("Collision Demos", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
}

int main(int argc, char* args[])
{
	init();

	bool quit = false, switchDemo = true;
	SDL_Event e;
	Square square;
	SDL_Rect square2;
	SDL_Surface* circle1ASurface = NULL; 
	SDL_Surface* circle1BSurface = NULL;
	SDL_Surface* circle2Surface = NULL;

	circle1ASurface = SDL_LoadBMP("circle1a.bmp");
	circle1BSurface = SDL_LoadBMP("circle1b.bmp");
	circle2Surface = SDL_LoadBMP("circle2.bmp");
	SDL_Texture* circle1ATexture = SDL_CreateTextureFromSurface(renderer, circle1ASurface);
	SDL_Texture* circle1BTexture = SDL_CreateTextureFromSurface(renderer, circle1BSurface);
	SDL_Texture* circle2Texture = SDL_CreateTextureFromSurface(renderer, circle2Surface);
	Circle circle1(50, 50, 100, 100, circle1ATexture, circle1BTexture);
	Circle circle2(300, 300, 200, 200, circle2Texture, circle1ATexture);

	square2.x = 300;
	square2.y = 40;
	square2.w = 200;
	square2.h = 200;

	while (!quit)
	{
		if (switchDemo)
		{
			while (SDL_PollEvent(&e) != 0)
			{
				if (e.type == SDL_QUIT)
				{
					quit = true;
				}
				if (e.type == SDL_KEYDOWN)
				{
					if (e.key.keysym.sym == SDLK_1)
					{
						switchDemo = true;
					}
					if (e.key.keysym.sym == SDLK_2)
					{
						switchDemo = false;
					}
				}
				square.HandleEvent(e);
			}

			square.Move(square2);

			SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
			SDL_RenderClear(renderer);
			SDL_SetRenderDrawColor(renderer, 0x00, 0xFF, 0x00, 0x00);
			SDL_RenderDrawRect(renderer, &square2);
			SDL_RenderFillRect(renderer, &square2);

			square.Render(renderer);
			SDL_RenderPresent(renderer);
		}
		else
		{
			while (SDL_PollEvent(&e) != 0)
			{
				if (e.type == SDL_QUIT)
				{
					quit = true;
				}
				circle1.HandleEvent(e);
				circle1.Move(circle2.GetCollider());

				SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
				SDL_RenderClear(renderer);

				circle2.Render(renderer);
				circle1.Render(renderer);

				SDL_RenderPresent(renderer);
				if (e.type == SDL_KEYDOWN)
				{
					if (e.key.keysym.sym == SDLK_1)
					{
						switchDemo = true;
					}
					if (e.key.keysym.sym == SDLK_2)
					{
						switchDemo = false;
					}
				}
			}
		}
		
	}
	return 0;
}