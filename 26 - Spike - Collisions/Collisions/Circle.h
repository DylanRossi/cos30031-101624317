#include <iostream>
#include <string>
#include <SDL.h>

using namespace std;

struct CircleStruct { int x, y; int r; };

class Circle
{
private:
	int posX, posY, velX, velY, cWidth, cHeight;
	CircleStruct collider;
	SDL_Texture* cTexture;
	SDL_Texture* cTextureA;
	SDL_Texture* cTextureB;
	void ShiftColliders();
public:
	static const int SCREEN_WIDTH = 700;
	static const int SCREEN_HEIGHT = 500;

	static const int CIRCLE_VELOCITY = 10;

	Circle(int x, int y, int width, int height, SDL_Texture* textureA, SDL_Texture* textureB);

	void HandleEvent(SDL_Event &e);
	void Move(CircleStruct& circle);
	void Render(SDL_Renderer* renderer);
	CircleStruct& GetCollider();
	bool CheckCollision(CircleStruct& b);
	double DistanceSquared(int x2, int y2);
};