#pragma once

#include <iostream>
#include <string>
#include <vector>
#include "Command.h"

using namespace std;

class World;
class CommandManager;

class OpenCommand : public Command
{
public:
	OpenCommand();
	void Execute(vector<string> aArgs, World* aWorld);
};