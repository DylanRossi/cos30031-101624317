#include <iostream>
#include <string>
#include "LookCommand.h"
#include "World.h"

using namespace std;

LookCommand::LookCommand()
{
}

void LookCommand::Execute(vector<string> aArgs, World* aWorld)
{
	if (aArgs.size() == 1)
	{
		map<int, Item*> lTempItems;
		for (map<int, Item*>::iterator it = aWorld->GetItems()->begin(); it != aWorld->GetItems()->end(); it++)
		{
			lTempItems.insert(pair<int, Item*>(it->first, it->second));
		}
		vector<int> lTempLocationItems = aWorld->GetCurrentLocation()->GetItemList();
		map<string, int> lTempEdges = aWorld->GetCurrentLocation()->GetEdges();
		map<string, int>::iterator it;
		cout << aWorld->GetCurrentLocation()->GetDescription() << endl;
		cout << "There are paths heading: ";
		for (it = lTempEdges.begin(); it != lTempEdges.end(); it++)
		{
			cout << it->first << " ";
		}
		cout << endl;
		cout << "You can see: " << endl;
		aWorld->GetCurrentLocation()->GetInventory()->ListInventory();
	}
	else if (aArgs.size() == 3)
	{
		if (aArgs[1] == "at")
		{
			string lTempSearchItem = aArgs[2];

			for (int j = 0; j < lTempSearchItem.size(); j++)
			{
				lTempSearchItem[j] = tolower(lTempSearchItem[j]);
			}
			Item* lTempItem = aWorld->GetCurrentLocation()->GetInventory()->FindItem(lTempSearchItem);
			if (lTempItem != NULL)
			{
				cout << lTempItem->GetDescription() << endl;
			}
		}
		else if (aArgs[1] == "in")
		{
			string lTempSearchItem = aArgs[2];

			for (int i = 0; i < lTempSearchItem.size(); i++)
			{
				lTempSearchItem[i] = tolower(lTempSearchItem[i]);
			}
			if (!aWorld->GetInventory()->ContainsItem(aArgs[2]))
			{
				cout << "You don't have a " << aArgs[2] << " to look in" << endl;
			}
			else
			{
				if (aWorld->GetInventory()->FindItem(aArgs[2])->IsOpenable())
				{
					cout << aWorld->GetInventory()->FindItem(aArgs[2])->GetName() << " is closed" << endl;
				}
				else
				{
					cout << aWorld->GetInventory()->FindItem(aArgs[2])->GetName() << " contains: " << endl;
					aWorld->GetInventory()->FindItem(aArgs[2])->GetInventory()->ListInventory();
				}
			}
		}
		else
		{
			cout << "Syntax: look at/in object" << endl;
		}
	}
	else
	{
		cout << "Syntax: look [at/in object]" << endl;
	}
}