#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include "Location.h"

using namespace std;

Location::Location()
{

}

Location::Location(string aLocationInfo)
{
	fInventory = new Inventory();
	ReadInfo(aLocationInfo);
}

void Location::ReadInfo(string aLocationInfo)
{
	char lDelim = ':';
	vector<string> lInfo;
	stringstream lSs(aLocationInfo);
	string lS;

	while (getline(lSs, lS, lDelim))
	{
		lInfo.push_back(lS);
	}

	fId = stoi(lInfo[1]);
	fName = lInfo[2];
	fDescription = lInfo[3];

	ReadEdges(lInfo[4]);
	if (lInfo.size() > 5)
	{
		ReadItems(lInfo[5]);
	}
}

void Location::ReadEdges(string aEdgeInfo)
{
	char lDelim = ',';
	vector<string> lEdgeVector;
	stringstream lSs(aEdgeInfo);
	string lS;

	while (getline(lSs, lS, lDelim))
	{
		lEdgeVector.push_back(lS);
	}

	lDelim = '=';

	for (int i = 0; i < lEdgeVector.size(); i++)
	{
		vector<string> lEdgeInfo;
		stringstream lSs2(lEdgeVector[i]);

		while (getline(lSs2, lS, lDelim))
		{
			lEdgeInfo.push_back(lS);
		}

		fEdges.insert(pair<string, int>(lEdgeInfo[0], stoi(lEdgeInfo[1])));
	}
}

void Location::ReadItems(string aItemInfo)
{
	char lDelim = ',';
	vector<string> lEdgeVector;
	stringstream lSs(aItemInfo);
	string lS;

	while (getline(lSs, lS, lDelim))
	{
		fItems.push_back(stoi(lS));
	}
}

map <string, int> Location::GetEdges()
{
	return fEdges;
}

string Location::GetName()
{
	return fName;
}

string Location::GetDescription()
{
	return fDescription;
}

vector<int> Location::GetItemList()
{
	return fItems;
}

Inventory* Location::GetInventory()
{
	return fInventory;
}