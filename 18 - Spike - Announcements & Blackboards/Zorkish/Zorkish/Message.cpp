#include <iostream>
#include <string>
#include "Message.h"

using namespace std;

Message::Message(string aToo, string aFrom, string aType, string aData)
{
	fToo = aToo;
	fFrom = aFrom;
	fType = aType;
	fData = aData;
}

string Message::GetToo()
{
	return fToo;
}

string Message::GetFrom()
{
	return fFrom;
}

string Message::GetType()
{
	return fType;
}

string Message::GetData()
{
	return fData;
}