#pragma once

#include <iostream>
#include <string>
#include <vector>
#include "Command.h"

using namespace std;

class World;
class CommandManager;

class AttackCommand : public Command
{
public:
	AttackCommand();
	void Execute(vector<string> aArgs, World* aWorld);
};