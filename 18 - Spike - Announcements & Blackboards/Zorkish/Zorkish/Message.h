#pragma once

#include <string>
#include <iostream>
#include <vector>

using namespace std;

class Message
{
private:
	string fToo, fFrom, fType, fData;
public:
	Message(string aToo, string aFrom, string aType, string aData);

	string GetToo();
	string GetFrom();
	string GetType();
	string GetData();
};