#include <iostream>
#include <string>
#include <map>
#include "DebugTreeCommand.h"
#include "World.h"

using namespace std;

DebugTreeCommand::DebugTreeCommand()
{
}

void DebugTreeCommand::Execute(vector<string> aArgs, World* aWorld)
{
	cout << "-----Debug Info-----" << endl;
	cout << "Current location: " << aWorld->GetCurrentLocation()->GetName() << endl;
	cout << aWorld->GetCurrentLocation()->GetDescription() << endl;
	cout << "Edges: " << endl;
	map<string, int> lTempEdges = aWorld->GetCurrentLocation()->GetEdges();
	map<string, int>::iterator it;
	map<int, Item*> lTempItems;
	for (map<int, Item*>::iterator it = aWorld->GetItems()->begin(); it != aWorld->GetItems()->end(); it++)
	{
		lTempItems.insert(pair<int, Item*>(it->first, it->second));
	}
	vector<int> lTempLocationItems = aWorld->GetCurrentLocation()->GetItemList();
	for (it = lTempEdges.begin(); it != lTempEdges.end(); it++)
	{
		cout << it->first << ": " << it->second << endl;
	}
	cout << "Location items: " << endl;
	for (int i = 0; i < lTempLocationItems.size(); i++)
	{
		cout << lTempItems.find(lTempLocationItems[i])->second->GetName() << ": " << lTempItems.find(lTempLocationItems[i])->second->GetDescription() << endl;;
	}
	cout << "Player's inventory: " << endl;
	aWorld->GetInventory()->ListInventory();
}