#include <iostream>
#include <string>
#include <map>
#include "PutCommand.h"
#include "CommandManager.h"

using namespace std;

PutCommand::PutCommand()
{
}

void PutCommand::Execute(vector<string> aArgs, World* aWorld)
{
	Item* lContainerItem = aWorld->GetInventory()->FindItem(aArgs[3]);
	if (lContainerItem == NULL)
	{
		lContainerItem = aWorld->GetCurrentLocation()->GetInventory()->FindItem(aArgs[3]);
		if (lContainerItem == NULL)
		{
			cout << "Can't find " << aArgs[3] << "to put item inside" << endl;
			return;
		}
	}
	if (!aWorld->GetInventory()->ContainsItem(aArgs[1]))
	{
		if (!aWorld->GetCurrentLocation()->GetInventory()->ContainsItem(aArgs[1]))
		{
			cout << "Can't find " << aArgs[1] << "to put inside" << aArgs[3] << endl;
			return;
		}
		else
		{
			if (aWorld->GetCurrentLocation()->GetInventory()->FindItem(aArgs[1])->IsInteractable())
			{
				lContainerItem->GetInventory()->AddItem(aWorld->GetCurrentLocation()->GetInventory()->TakeItem(aArgs[1]));
			}
			else
			{
				cout << "You can't move the " << aArgs[1] << endl;
				return;
			}
		}
	}
	else
	{
		if (aWorld->GetInventory()->FindItem(aArgs[1])->IsInteractable())
		{
			lContainerItem->GetInventory()->AddItem(aWorld->GetInventory()->TakeItem(aArgs[1]));
		}
		else
		{
			cout << "You can't move the " << aArgs[1] << endl;
			return;
		}
	}
	cout << aArgs[1] << " has been put inside " << aArgs[3] << endl;
}