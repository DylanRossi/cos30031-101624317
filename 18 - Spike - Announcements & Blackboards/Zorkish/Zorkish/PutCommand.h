#pragma once

#include <iostream>
#include <string>
#include <vector>
#include "Command.h"

using namespace std;

class World;

class PutCommand : public Command
{
public:
	PutCommand();
	void Execute(vector<string> aArgs, World* aWorld);
};