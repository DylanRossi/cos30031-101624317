#pragma once

#include <iostream>
#include <string>
#include <vector>
#include "Command.h"

using namespace std;

class World;

class InventoryCommand : public Command
{
public:
	InventoryCommand();
	void Execute(vector<string> aArgs, World* aWorld);
};