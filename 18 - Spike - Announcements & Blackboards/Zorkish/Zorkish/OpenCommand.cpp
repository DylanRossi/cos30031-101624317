#include <iostream>
#include <string>
#include <map>
#include "OpenCommand.h"
#include "CommandManager.h"

using namespace std;

OpenCommand::OpenCommand()
{
}

void OpenCommand::Execute(vector<string> aArgs, World* aWorld)
{
	if (aWorld->GetInventory()->ContainsItem(aArgs[1]))
	{
		Message lMessage = Message(aWorld->GetInventory()->FindItem(aArgs[1])->GetName(), aWorld->GetName(), "open", "");
		aWorld->GetMessageSystem()->AddMessage(lMessage);
	}
	else
	{
		cout << "You don't have a " << aArgs[1] << endl;
	}
}