#include <iostream>
#include <string>

using namespace std;

class Damage
{
private:
	int fValue;
public:
	Damage(string aValue);

	int GetValue();
};