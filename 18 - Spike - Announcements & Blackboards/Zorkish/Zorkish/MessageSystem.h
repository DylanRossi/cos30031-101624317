#pragma once

#include <iostream>
#include <string>
#include <vector>
#include "Message.h"

using namespace std;

class World;

class MessageSystem
{
private:
	vector<Message> fMessageQueue;
public:
	MessageSystem();

	void AddMessage(Message aMessage);
	void DeliverMessages(World* aWorld);
};