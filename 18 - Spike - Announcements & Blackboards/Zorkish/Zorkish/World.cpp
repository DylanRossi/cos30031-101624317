#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <sstream>
#include "World.h"

using namespace std;

World::World()
{
}

World::World(vector<string> aWorldArray)
{
	fItems = new map<int, Item*>();
	ReadInfo(aWorldArray[0]);

	for (int i = 1; i < fLocationNo; i++)
	{
		fLocations.insert(pair<int, Location*>(i - 1, new Location(aWorldArray[i])));
	}

	for (int i = 0; i < fItemNo; i++)
	{
		fItems->insert(pair<int, Item*>(i, new Item(aWorldArray[i + fLocationNo + 1])));
	}

	map<int, Item*> tempItems;
	for (map<int, Item*>::iterator it = fItems->begin(); it != fItems->end(); it++)
	{
		tempItems.insert(pair<int, Item*>(it->first, it->second));
	}

	for (int i = 0; i < tempItems.size(); i++)
	{
		for (int j = 0; j < tempItems[i]->GetItemList().size(); j++)
		{
			tempItems[i]->GetInventory()->AddItem(tempItems[tempItems[i]->GetItemList()[j]]);
		}
	}

	for (map<int, Location*>::iterator it = fLocations.begin(); it != fLocations.end(); it++)
	{
		for (int i = 0; i < it->second->GetItemList().size(); i++)
		{
			it->second->GetInventory()->AddItem(tempItems[it->second->GetItemList()[i]]);
		}
	}

	fCurrentLocation = fLocations[0];
	fCommandManager = new CommandManager();
	fInventory = new Inventory();
	fMessageSystem = new MessageSystem();
}

World::~World()
{

}

void World::ReadInfo(string aInfoArray)
{
	char lDelim = ':';
	vector<string> lInfo;
	stringstream lSs(aInfoArray);
	string lS;

	while (getline(lSs, lS, lDelim))
	{
		lInfo.push_back(lS);
	}

	fId = stoi(lInfo[1]);
	fName = lInfo[2];
	fDescription = lInfo[3];
	fLocationNo = stoi(lInfo[4]);
	fItemNo = stoi(lInfo[5]);
}

void World::Handle(StageManager* aStageManager)
{
	string lAnswer;

	cout << "Welcome to Zorkish: " << fName << endl;
	cout << fDescription << endl;

	getline(cin, lAnswer);
	ProcessTurn(aStageManager);
}

void World::ProcessTurn(StageManager* aStageManager)
{
	string lAnswer;

	cout << ":> ";
	getline(cin, lAnswer);

	if (lAnswer.find("go") != string::npos)
	{
		ProcessGoCommand(lAnswer, aStageManager);
	}
	else if (lAnswer == "hiscore")
	{
		cout << "You have entered the magic word and will now see the New High Score screen." << endl;
		aStageManager->ChangeStage(StageManager::STAGE_HIGHSCORE);
		aStageManager->Handle();
	}
	else if (lAnswer == "quit")
	{
		cout << "Your adventure has ended without fame or fortune." << endl;
		aStageManager->ChangeStage(StageManager::STAGE_MAINMENU);
		aStageManager->Handle();
	}
	else
	{
		fCommandManager->Execute(lAnswer, this);
		fMessageSystem->DeliverMessages(this);
		ProcessTurn(aStageManager);
	}
}

void World::ProcessGoCommand(string aAnswer, StageManager* aStageManager)
{
	string lDirection = aAnswer.substr(3);
	map <string, int> lEdges = fCurrentLocation->GetEdges();

	if (lEdges.find(lDirection) == lEdges.end())
	{
		cout << "You can't move in that direction" << endl;
	}
	else
	{
		fCurrentLocation = fLocations[lEdges[lDirection]];
		cout << "You arrive at " << fCurrentLocation->GetName() << endl;
		cout << fCurrentLocation->GetDescription() << endl;
	}
	ProcessTurn(aStageManager);
}

string World::GetName()
{
	return fName;
}

Inventory* World::GetInventory()
{
	return fInventory;
}

Location* World::GetCurrentLocation()
{
	return fCurrentLocation;
}

CommandManager* World::GetCommandManager()
{
	return fCommandManager;
}

int World::GetNoLocations()
{
	return fLocationNo;
}

int World::GetNoItems()
{
	return fItemNo;
}

map<int, Item*>* World::GetItems()
{
	return fItems;
}

MessageSystem* World::GetMessageSystem()
{
	return fMessageSystem;
}