#include <string>
#include "Inventory.h"
#include "Item.h"

using namespace std;

void Inventory::AddItem(Item* aItem)
{
	fInventory.push_back(aItem);
}

Item* Inventory::TakeItem(string aItemName)
{
	string lTempItemName=aItemName;
	for (int i = 0; i < lTempItemName.size(); i++)
	{
		lTempItemName[i] = tolower(lTempItemName[i]);
	}
	for (int i = 0; i < fInventory.size(); i++)
	{
		string lTempSearchName = fInventory[i]->GetName();
		for (int j = 0; j < lTempSearchName.size(); j++)
		{
			lTempSearchName[j] = tolower(lTempSearchName[j]);
		}
		if (lTempSearchName == lTempItemName)
		{
			Item* lSelection = fInventory[i];
			fInventory.erase(fInventory.begin() + i);
			return lSelection;
		}
	}
	//cout << "No match found" << endl;
	return NULL;
}

Item* Inventory::FindItem(string aItemName)
{
	string lTempItemName = aItemName;
	for (int i = 0; i < lTempItemName.size(); i++)
	{
		lTempItemName[i] = tolower(lTempItemName[i]);
	}
	for (int i = 0; i < fInventory.size(); i++)
	{
		string lTempName = fInventory[i]->GetName();
		for (int i = 0; i < lTempName.size(); i++)
		{
			lTempName[i] = tolower(lTempName[i]);
		}
		if (lTempName == lTempItemName)
		{
			return fInventory[i];
		}
	}
	//cout << "No item found" << endl;
	return NULL;
}

bool Inventory::ContainsItem(string aItemName)
{
	string lTempItemName = aItemName;
	for (int i = 0; i < lTempItemName.size(); i++)
	{
		lTempItemName[i] = tolower(lTempItemName[i]);
	}
	for (int i = 0; i < fInventory.size(); i++)
	{
		string lTempName = fInventory[i]->GetName();
		for (int i = 0; i < lTempName.size(); i++)
		{
			lTempName[i] = tolower(lTempName[i]);
		}
		if (lTempName == lTempItemName)
		{
			return true;
		}
	}
	//cout << "No item found" << endl;
	return false;
}

void Inventory::ListInventory()
{
	for (int i = 0; i < fInventory.size(); i++)
	{
		cout << fInventory[i]->GetName() << ": " << fInventory[i]->GetDescription() << endl;
	}
}

