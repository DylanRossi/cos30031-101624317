#include <iostream>
#include <string>
#include <map>
#include "MessageSystem.h"
#include "World.h"

using namespace std;

MessageSystem::MessageSystem()
{

}

void MessageSystem::AddMessage(Message aMessage)
{
	fMessageQueue.push_back(aMessage);
}

void MessageSystem::DeliverMessages(World* aWorld)
{
	for (int i = 0; i < fMessageQueue.size(); i++)
	{
		Message tempMessage = fMessageQueue[i];
		map<int, Item*>::iterator it2;
		for (it2 = aWorld->GetItems()->begin(); it2 != aWorld->GetItems()->end(); it2++)
		{
			if (it2->second->GetName() == tempMessage.GetToo())
			{
				it2->second->ReadMessage(tempMessage);
				fMessageQueue.erase(fMessageQueue.begin()+i);
			}
		}
	}
}