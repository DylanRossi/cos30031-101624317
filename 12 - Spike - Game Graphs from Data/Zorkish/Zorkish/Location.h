#pragma once

#include<iostream>
#include <string>
#include <map>

using namespace std;

class Location
{
private:
	int fId;
	string fName;
	string fDescription;
	map <string, int> fEdges;

public:
	Location();
	Location(string aLocationInfo);

	void ReadInfo(string aLocationInfo);
	void ReadEdges(string aEdgeInfo);

	map <string, int> GetEdges();
	string GetName();
	string GetDescription();
};