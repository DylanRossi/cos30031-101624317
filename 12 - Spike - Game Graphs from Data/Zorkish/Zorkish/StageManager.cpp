#include <iostream>
#include <string>
#include "StageManager.h"

using namespace std;

StageManager::StageManager() : fStage(new MainMenu())
{
}

StageManager::~StageManager()
{
	delete fStage;
}

void StageManager::Handle()
{
	fStage->Handle(this);
}

void StageManager::ChangeStage(Stages aStage)
{
	delete fStage;

	switch (aStage)
	{
	case Stages::STAGE_MAINMENU:
		fStage = new MainMenu();
		break;
	case Stages::STAGE_ABOUT:
		fStage = new About();
		break;
	case Stages::STAGE_HELP:
		fStage = new Help();
		break;
	case Stages::STAGE_SELECTADVENTURE:
		fStage = new SelectAdventure();
		break;
	case Stages::STAGE_HIGHSCORE:
		fStage = new HighScore();
		break;
	case Stages::STAGE_HOF:
		fStage = new HOF();
		break;
	default:
		cout << "You broke something..." << endl;
		break;
	}

	cout << endl;
}