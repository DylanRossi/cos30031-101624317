#pragma once

#include <iostream>
#include <string>
#include <fstream>
#include <map>
#include <vector>
#include "Location.h"
#include "Stage.h"
#include "StageManager.h"

using namespace std;

class World : public Stage
{
private:
	map <int, Location*> fLocations;
	Location* fCurrentLocation;
	string fDescription;
	string fName;
	int fId;

public:
	World(vector<string> aWorldArray);
	virtual ~World();

	void ReadInfo(string aInfoArray);
	virtual void Handle(StageManager* aStageManager);
	void ProcessGoCommand(string aAnswer, StageManager* aStageManager);
	void ProcessTurn(StageManager* aStageManager);

	string GetName();
};