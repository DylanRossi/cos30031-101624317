#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <sstream>
#include "World.h"

using namespace std;

World::World(vector<string> aWorldArray)
{
	ReadInfo(aWorldArray[0]);
	
	for (int i = 1; i < aWorldArray.size(); i++)
	{
		fLocations.insert(pair<int, Location*>(i-1, new Location(aWorldArray[i])));
	}

	fCurrentLocation = fLocations[0];
}

World::~World()
{

}

void World::ReadInfo(string aInfoArray)
{
	char lDelim = ':';
	vector<string> lInfo;
	stringstream lSs(aInfoArray);
	string lS;

	while (getline(lSs, lS, lDelim))
	{
		lInfo.push_back(lS);
	}

	fId = stoi(lInfo[1]);
	fName = lInfo[2];
	fDescription = lInfo[3];
}

void World::Handle(StageManager* aStageManager)
{
	string lAnswer;

	cout << "Welcome to Zorkish: " << fName << endl;
	cout << fDescription << endl;

	getline(cin, lAnswer);
	ProcessTurn(aStageManager);
}

void World::ProcessTurn(StageManager* aStageManager)
{
	string lAnswer;

	cout << ":> ";
	getline(cin, lAnswer);

	if (lAnswer.find("go") != string::npos)
	{
		ProcessGoCommand(lAnswer, aStageManager);
	}
	else if (lAnswer == "hiscore")
	{
		cout << "You have entered the magic word and will now see the New High Score screen." << endl;
		aStageManager->ChangeStage(StageManager::STAGE_HIGHSCORE);
		aStageManager->Handle();
	}
	else
	{
		cout << "Your adventure has ended without fame or fortune." << endl;
		aStageManager->ChangeStage(StageManager::STAGE_MAINMENU);
		aStageManager->Handle();
	}
}

void World::ProcessGoCommand(string aAnswer, StageManager* aStageManager)
{
	string lDirection = aAnswer.substr(3);
	map <string, int> lEdges = fCurrentLocation->GetEdges();

	if (lEdges.find(lDirection)==lEdges.end())
	{
		cout << "You can't move in that direction" << endl;
	}
	else
	{
		fCurrentLocation = fLocations[lEdges[lDirection]];
		cout << "You arrive at " << fCurrentLocation->GetName() << endl;
		cout << fCurrentLocation->GetDescription() << endl;
	}

	ProcessTurn(aStageManager);
}

string World::GetName()
{
	return fName;
}

