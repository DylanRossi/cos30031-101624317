#pragma once

#include <iostream>
#include <string>
#include <vector>
#include "Stage.h"
#include "StageManager.h"

using namespace std;

class World;

class SelectAdventure : public Stage
{
private:
	vector <World> fWorlds;

public:
	SelectAdventure();
	virtual ~SelectAdventure();

	virtual void Handle(StageManager* aStageManager);
};