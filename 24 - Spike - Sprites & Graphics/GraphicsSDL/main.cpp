#include <SDL.h>
#include <iostream>

using namespace std;

const int SCREEN_WIDTH = 1000;
const int SCREEN_HEIGHT = 600;

int main(int argc, char * argv[])
{
	SDL_Window* window = NULL;
	SDL_Surface* stars = NULL;
	SDL_Surface* square = NULL;
	SDL_Texture * background;
	SDL_Event e;
	SDL_Rect squares[3];
	SDL_Rect squareRenderer1 = { 250, 300,0,0 };
	SDL_Rect squareRenderer2 = { 500, 300,0,0 };
	SDL_Rect squareRenderer3 = { 750, 300, 0,0 };
	bool quit = false, switch1 = true, switch2 = true, switch3 = true;

	for (int i = 0; i < 3; i++)
	{
		squares[i].w = 100;
		squares[i].h = 100;
	}

	squares[0].x = 25;
	squares[0].y = 25;
	squares[1].x = 160;
	squares[1].y = 25;
	squares[2].x = 160;
	squares[2].y = 160;

	SDL_Init(SDL_INIT_EVERYTHING);
	window = SDL_CreateWindow("Sprite Testing", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	stars = SDL_LoadBMP("stars2.bmp");
	square = SDL_LoadBMP("squares.bmp");

	SDL_Renderer * renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_Texture * starTexture = SDL_CreateTextureFromSurface(renderer, stars);
	SDL_Texture * squareTexture = SDL_CreateTextureFromSurface(renderer, square);

	background = starTexture;

	squareRenderer1.w = squares[0].w;
	squareRenderer1.h = squares[0].h;
	squareRenderer2.w = squares[1].w;
	squareRenderer2.h = squares[1].h;
	squareRenderer3.w = squares[2].w;
	squareRenderer3.h = squares[2].h;

	while (!quit)
	{
		while (SDL_PollEvent(&e))
		{
			if (e.type == SDL_QUIT)
			{
				quit = true;
			}
			if (e.type == SDL_KEYDOWN)
			{
				switch (e.key.keysym.sym)
				{
				case SDLK_0:
					if (background == NULL)
					{

						background = starTexture;
					}
					else
					{
						background = NULL;
					}
					break;
				case SDLK_1:
					if (switch1)
					{
						squareRenderer1.x = 100;
						squareRenderer1.y = 500;
					}
					else
					{
						squareRenderer1.x = 250;
						squareRenderer1.y = 300;
					}
					switch1 = !switch1;
					break;
				case SDLK_2:
					if (switch2)
					{
						squareRenderer2.x = 800;
						squareRenderer2.y = 25;
					}
					else
					{
						squareRenderer2.x = 500;
						squareRenderer2.y = 300;
					}
					switch2 = !switch2;
					break;
				case SDLK_3:
					if (switch3)
					{
						squareRenderer3.x = 349;
						squareRenderer3.y = 459;
					}
					else
					{
						squareRenderer3.x = 750;
						squareRenderer3.y = 300;
					}
					switch3 = !switch3;
					break;
				}
			}
		}
		SDL_RenderClear(renderer);
		SDL_RenderCopy(renderer, background, NULL, NULL);
		SDL_RenderCopy(renderer, squareTexture, &squares[0], &squareRenderer1);
		SDL_RenderCopy(renderer, squareTexture, &squares[1], &squareRenderer2);
		SDL_RenderCopy(renderer, squareTexture, &squares[2], &squareRenderer3);
		SDL_RenderPresent(renderer);
	}

	return 0;
}