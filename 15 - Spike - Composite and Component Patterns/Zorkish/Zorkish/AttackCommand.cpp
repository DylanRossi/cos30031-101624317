#include <iostream>
#include <string>
#include <map>
#include "AttackCommand.h"
#include "CommandManager.h"

using namespace std;

AttackCommand::AttackCommand()
{
}

void AttackCommand::Execute(vector<string> aArgs, World* aWorld)
{
	if (aArgs.size() == 2)
	{
		if (aWorld->GetCurrentLocation()->GetInventory()->ContainsItem(aArgs[1]))
		{
			aWorld->GetCurrentLocation()->GetInventory()->FindItem(aArgs[1])->Attacked(-5, "fists");
		}
		else
		{
			cout << "There is no " << aArgs[1] << " to attack" << endl;
		}
	}
	if (aArgs.size() == 4)
	{
		if (aWorld->GetCurrentLocation()->GetInventory()->ContainsItem(aArgs[1]))
		{
			if (!aWorld->GetInventory()->ContainsItem(aArgs[3]))
			{
				cout << "You don't have a " << aArgs[3] << " to attack with" << endl;
			}
			else
			{
				if (aWorld->GetInventory()->FindItem(aArgs[3])->CanDamage())
				{
					aWorld->GetCurrentLocation()->GetInventory()->FindItem(aArgs[1])->Attacked(aWorld->GetInventory()->FindItem(aArgs[3])->GetDamage(), aArgs[3]);
				}
				else
				{
					cout << "You can't attack with a " << aArgs[3] << endl;
				}
			}
		}
		else
		{
			cout << "There is no " << aArgs[1] << " to attack" << endl;
		}
	}
}