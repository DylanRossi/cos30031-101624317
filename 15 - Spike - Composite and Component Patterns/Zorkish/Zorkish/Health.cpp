#include <iostream>
#include <string>
#include "Health.h"

using namespace std;

Health::Health(string aValue)
{
	fValue = stoi(aValue);
}

int Health::GetValue()
{
	return fValue;
}

void Health::ModifyHealth(int aValue)
{
	fValue += aValue;
}