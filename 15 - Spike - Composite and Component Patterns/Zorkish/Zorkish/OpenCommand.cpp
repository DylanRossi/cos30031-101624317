#include <iostream>
#include <string>
#include <map>
#include "OpenCommand.h"
#include "CommandManager.h"

using namespace std;

OpenCommand::OpenCommand()
{
}

void OpenCommand::Execute(vector<string> aArgs, World* aWorld)
{
	if (aWorld->GetInventory()->ContainsItem(aArgs[1]))
	{
		aWorld->GetInventory()->FindItem(aArgs[1])->Open();
	}
	else
	{
		cout << "You don't have a " << aArgs[1] << endl;
	}
}