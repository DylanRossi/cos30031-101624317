#pragma once

#include <iostream>
#include <string>
#include "Stage.h"
#include "StageManager.h"

using namespace std;

class HOF : public Stage
{
public:
	HOF();
	virtual ~HOF();

	virtual void Handle(StageManager* aStageManager);
};