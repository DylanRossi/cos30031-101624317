#include <iostream>
#include <string>

using namespace std;

class Health
{
private:
	int fValue;
public:
	Health(string aValue);

	void ModifyHealth(int aValue);

	int GetValue();
};