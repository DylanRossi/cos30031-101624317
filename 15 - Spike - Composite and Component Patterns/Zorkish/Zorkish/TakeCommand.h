#pragma once

#include <iostream>
#include <string>
#include <vector>
#include "Command.h"

using namespace std;

class World;

class TakeCommand : public Command
{
public:
	TakeCommand();
	void Execute(vector<string> aArgs, World* aWorld);
};