#pragma once

#include <vector>
#include <iostream>

using namespace std;

class Item;

class Inventory
{
private:
	vector<Item*> fInventory;
public:
	void AddItem(Item* aItem);
	Item* TakeItem(string aItemName);
	Item* FindItem(string aItemName);
	bool ContainsItem(string aItemName);
	void ListInventory();
};