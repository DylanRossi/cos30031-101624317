#include <iostream>
#include <string>
#include "CommandInvoker.h"

using namespace std;

void CommandInvoker::AddCommand(Command aCommand)
{
	fCommandQueue.push_back(aCommand);
}

void CommandInvoker::InvokeCommands(World aWorld, string aArgs[])
{
	list<Command>::iterator it;
	for (it = fCommandQueue.begin(); it != fCommandQueue.end(); ++it)
	{
		it->Execute(aWorld, aArgs);
	}
	fCommandQueue.clear();
}