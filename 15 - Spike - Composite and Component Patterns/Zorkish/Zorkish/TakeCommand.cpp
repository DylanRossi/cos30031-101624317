#include <iostream>
#include <string>
#include <map>
#include "TakeCommand.h"
#include "CommandManager.h"

using namespace std;

TakeCommand::TakeCommand()
{
}

void TakeCommand::Execute(vector<string> aArgs, World* aWorld)
{
	if (aArgs.size() == 2)
	{
		if (!aWorld->GetCurrentLocation()->GetInventory()->ContainsItem(aArgs[1]))
		{
			cout << "I can't find " << aArgs[1] << endl;
		}
		else
		{
			if (aWorld->GetCurrentLocation()->GetInventory()->FindItem(aArgs[1])->IsInteractable())
			{
				aWorld->GetInventory()->AddItem(aWorld->GetCurrentLocation()->GetInventory()->TakeItem(aArgs[1]));
				cout << "You have picked up the " << aArgs[1] << endl;
			}
			else
			{
				cout << "You can't pick up the " << aArgs[1] << endl;
			}
		}
	}
	if (aArgs.size() == 4)
	{
		Item* lContainerItem = aWorld->GetInventory()->FindItem(aArgs[3]);
		if (lContainerItem == NULL)
		{
			lContainerItem = aWorld->GetCurrentLocation()->GetInventory()->FindItem(aArgs[3]);
			if (lContainerItem == NULL)
			{
				cout << "Can't find " << aArgs[3] << "to look in" << endl;
				return;
			}
		}
		Item* lTakenItem = lContainerItem->GetInventory()->FindItem(aArgs[1]);
		if (lTakenItem == NULL)
		{
			cout << "I can't find " << aArgs[1] << " inside " << aArgs[3] << endl;
		}
		else
		{
			aWorld->GetInventory()->AddItem(lContainerItem->GetInventory()->TakeItem(aArgs[1]));
			cout << aArgs[1] << " has been taken from " << aArgs[3] << endl;
		}
	}
}