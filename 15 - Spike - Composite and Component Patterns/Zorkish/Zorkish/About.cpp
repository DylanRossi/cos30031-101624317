#include <iostream>
#include <string>
#include "About.h"

using namespace std;

About::About() : Stage()
{

}

About::~About()
{

}

void About::Handle(StageManager* aStageManager)
{
	cout << "Zorkish :: About" << endl;
	cout << "--------------------------------------------------------" << endl;
	cout << endl;
	cout << "Written by: Dylan Rossi" << endl;
	cout << endl;
	cout << "Press Enter to return to the Main Menu" << endl;
	cin.get();

	if (cin.get())
	{
		aStageManager->ChangeStage(StageManager::STAGE_MAINMENU);
		aStageManager->Handle();
	}
}