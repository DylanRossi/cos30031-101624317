#pragma once

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class World;

class Command
{
public:
	virtual void Execute(vector<string> aArgs, World* aWorld);
};