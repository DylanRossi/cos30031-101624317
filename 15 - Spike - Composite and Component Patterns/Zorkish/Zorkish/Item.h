#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include "Inventory.h"
#include "Openable.h"
#include "Attackable.h"
#include "Damage.h"
#include "Health.h"
#include "Interactable.h"

using namespace std;

class Item
{
private:
	string fName, fDescription;
	int fId;
	Inventory* fInventory;
	Openable* fOpenable;
	Attackable* fAttackable;
	Health* fHealth;
	Damage* fDamage;
	Interactable* fInteractable;
	vector<int> fItems;
public:
	Item(string aItemInfo);
	void ReadItems(string aItemInfo);
	void Open();
	void Attacked(int aDamage, string aItemName);

	string GetName();
	string GetDescription();
	int GetId();
	Inventory* GetInventory();
	vector<int> GetItemList();
	Health* GetHealth();
	int GetDamage();
	bool IsOpenable();
	bool IsAttackable();
	bool IsInteractable();
	bool CanDamage();

};