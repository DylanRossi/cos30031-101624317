#include <iostream>
#include <string>
#include "HelpCommand.h"

using namespace std;

HelpCommand::HelpCommand()
{
}

void HelpCommand::Execute(vector<string> aArgs, World* aWorld)
{
	cout << "[go] _, (or just n, ne, e, etc)" << endl;
	cout << "look," << endl;
	cout << "look at _," << endl;
	cout << "inventory," << endl;
	cout << "debug tree," << endl;
	cout << "hiscore," << endl;
	cout << "alias 'new keyword' 'existing command'," << endl;
	cout << "quit" << endl;
	cout << "[up arrow] to repeat last command" << endl;
}