#pragma once

#include <iostream>
#include <string>
#include "Stage.h"
#include "StageManager.h"

using namespace std;

class HighScore : public Stage
{
public:
	HighScore();
	virtual ~HighScore();

	virtual void Handle(StageManager* aStageManager);
};