#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include "Item.h"

using namespace std;

Item::Item(string aItemInfo)
{
	fInventory = new Inventory();
	char lDelim = ':';
	vector<string> lInfo;
	stringstream lSs(aItemInfo);
	string lS;

	while (getline(lSs, lS, lDelim))
	{
		lInfo.push_back(lS);
	}
	fId = stoi(lInfo[1]);
	fName = lInfo[2];
	fDescription = lInfo[3];
	if (lInfo[4] != "")
	{
		ReadItems(lInfo[4]);
	}
	if (lInfo[5] !="")
	{
		fOpenable = new Openable();
	}
	else
	{
		fOpenable = NULL;
	}
	if (lInfo[6] != "")
	{
		fAttackable = new Attackable();
	}
	else
	{
		fAttackable = NULL;
	}
	if (lInfo[7] != "")
	{
		fInteractable = new Interactable();
	}
	else
	{
		fInteractable = NULL;
	}
	if (lInfo[8] != "")
	{
		fHealth = new Health(lInfo[8]);
	}
	else
	{
		fHealth = NULL;
	}
	if (lInfo[9] != "")
	{
		fDamage = new Damage(lInfo[9]);
	}
	else
	{
		fDamage = NULL;
	}
}

void Item::ReadItems(string aItemInfo)
{
	char lDelim = ',';
	stringstream lSs(aItemInfo);
	string lS;

	while (getline(lSs, lS, lDelim))
	{
		fItems.push_back(stoi(lS));
	}
}

void Item::Open()
{
	if (IsOpenable())
	{
		fOpenable = NULL;
		cout << "You have opened the " << fName << endl;
	}
	else
	{
		cout << "You can't open the " << fName << endl;
	}
}

void Item::Attacked(int aDamage, string aItemName)
{
	if (IsAttackable())
	{
		cout << "You attack " << fName << " with " << aItemName << endl;
		int lRand = rand() % 2;
		if (lRand == 0)
		{
			fHealth->ModifyHealth(aDamage);
			cout << "The " << fName << "'s health is now " << fHealth->GetValue() << "%" << endl;
			if (fHealth->GetValue() <= 0)
			{
				cout << "You have defeated the " << fName << endl;
				fAttackable = NULL;
			}
		}
		else
		{
			cout << "You miss the " << fName << endl;
		}
	}
	else
	{
		cout << "You can't attack the " << fName << endl;
	}
}

string Item::GetName()
{
	return fName;
}

string Item::GetDescription()
{
	return fDescription;
}

int Item::GetId()
{
	return fId;
}

Inventory* Item::GetInventory()
{
	return fInventory;
}

vector<int> Item::GetItemList()
{
	return fItems;
}

bool Item::IsOpenable()
{
	return fOpenable != NULL;
}

bool Item::IsAttackable()
{
	return fAttackable != NULL;
}

bool Item::IsInteractable()
{
	return fInteractable != NULL;
}

Health* Item::GetHealth()
{
	return fHealth;
}

bool Item::CanDamage()
{
	return fDamage != NULL;
}

int Item::GetDamage()
{
	return fDamage->GetValue();
}