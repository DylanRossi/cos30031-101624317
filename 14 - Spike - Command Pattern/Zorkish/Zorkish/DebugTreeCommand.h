#pragma once

#include <iostream>
#include <string>
#include <vector>
#include "Command.h"

using namespace std;

class World;

class DebugTreeCommand : public Command
{
public:
	DebugTreeCommand();
	void Execute(vector<string> aArgs, World* aWorld);
};