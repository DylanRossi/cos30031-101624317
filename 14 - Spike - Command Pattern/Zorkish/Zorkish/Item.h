#pragma once

#include <iostream>
#include <string>

using namespace std;

class Item
{
private:
	string fName, fDescription;
	int fId;
public:
	Item(string aItemInfo);

	string GetName();
	string GetDescription();
	int GetId();
};