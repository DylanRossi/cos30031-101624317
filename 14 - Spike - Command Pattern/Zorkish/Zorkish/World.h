#pragma once

#include <iostream>
#include <string>
#include <fstream>
#include <map>
#include <vector>
#include "Location.h"
#include "Stage.h"
#include "StageManager.h"
#include "Inventory.h"
#include "CommandManager.h"

using namespace std;

class World : public Stage
{
private:
	map <int, Location*> fLocations;
	map<int, Item*> fItems;
	Location* fCurrentLocation;
	string fDescription, fName;
	int fId, fLocationNo, fItemNo;
	Inventory fInventory;
	CommandManager* fCommandManager;
public:
	World();
	World(vector<string> aWorldArray);
	virtual ~World();

	void ReadInfo(string aInfoArray);
	virtual void Handle(StageManager* aStageManager);
	void ProcessGoCommand(string aAnswer, StageManager* aStageManager);
	void ProcessTurn(StageManager* aStageManager);

	string GetName();
	Inventory GetInventory();
	Location* GetCurrentLocation();
	CommandManager* GetCommandManager();
	int GetNoLocations();
	int GetNoItems();
	map<int, Item*> GetItems();
};