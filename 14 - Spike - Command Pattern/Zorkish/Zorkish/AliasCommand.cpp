#include <iostream>
#include <string>
#include <map>
#include "AliasCommand.h"
#include "CommandManager.h"

using namespace std;

AliasCommand::AliasCommand()
{
}

void AliasCommand::Execute(vector<string> aArgs, World* aWorld)
{
	if (aArgs.size() != 3)
	{
		cout << "Syntax: alias 'new keyword' 'existing command'" << endl;
		return;
	}
	
	multimap<string, Command*>* lTempCommandList = aWorld->GetCommandManager()->GetCommandList();
	multimap<string, Command*>::iterator it = lTempCommandList->find(aArgs[1]);
	if (it != lTempCommandList->end())
	{
		cout << "Command alias already exists" << endl;
		return;
	}
	
	it = lTempCommandList->find(aArgs[2]);
	if (it == lTempCommandList->end())
	{
		cout << "Command doesn't exist" << endl;
		return;
	}

	for (it = lTempCommandList->begin(); it != lTempCommandList->end(); it++)
	{
		if (it->first == aArgs[2])
		{
			aWorld->GetCommandManager()->GetCommandList()->insert(pair<string, Command*>(aArgs[1], it->second));
		}
	}
}