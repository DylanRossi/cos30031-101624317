#pragma once

#include <iostream>
#include <string>
#include <vector>
#include "Command.h"

using namespace std;

class World;
class CommandManager;

class AliasCommand : public Command
{
public:
	AliasCommand();
	void Execute(vector<string> aArgs, World* aWorld);
};