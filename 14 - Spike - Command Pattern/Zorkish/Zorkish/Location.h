#pragma once

#include<iostream>
#include <string>
#include <map>
#include <vector>
#include "Item.h"

using namespace std;

class Location
{
private:
	int fId;
	string fName;
	string fDescription;
	map <string, int> fEdges;
	vector<int> fItems;

public:
	Location();
	Location(string aLocationInfo);

	void ReadInfo(string aLocationInfo);
	void ReadEdges(string aEdgeInfo);
	void ReadItems(string aItemInfo);

	map <string, int> GetEdges();
	string GetName();
	string GetDescription();
	vector<int> GetItemList();
};