#pragma once

#include <iostream>
#include <string>
#include <map>
#include "AliasCommand.h"
#include "DebugTreeCommand.h"
#include "HelpCommand.h"
#include "InventoryCommand.h"
#include "LookCommand.h"
#include "World.h"

using namespace std;

class CommandManager
{
private:
	multimap<string, Command*>* fCommands;
public:
	CommandManager();
	void Execute(string aInput, World* aWorld);

	multimap<string, Command*>* GetCommandList();
};