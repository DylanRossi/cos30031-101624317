#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include "Item.h"

using namespace std;

Item::Item(string aItemInfo)
{
	char lDelim = ':';
	vector<string> lInfo;
	stringstream lSs(aItemInfo);
	string lS;

	while (getline(lSs, lS, lDelim))
	{
		lInfo.push_back(lS);
	}

	fId = stoi(lInfo[1]);
	fName = lInfo[2];
	fDescription = lInfo[3];
}

string Item::GetName()
{
	return fName;
}

string Item::GetDescription()
{
	return fDescription;
}

int Item::GetId()
{
	return fId;
}