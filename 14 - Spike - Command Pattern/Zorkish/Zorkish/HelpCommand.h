#pragma once

#include <iostream>
#include <string>
#include <vector>
#include "Command.h"

using namespace std;

class World;

class HelpCommand : public Command
{
public:
	HelpCommand();
	void Execute(vector<string> aArgs, World* aWorld);
};