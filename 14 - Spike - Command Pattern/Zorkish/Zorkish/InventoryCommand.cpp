#include <iostream>
#include <string>
#include "InventoryCommand.h"
#include "World.h"

using namespace std;

InventoryCommand::InventoryCommand()
{
}

void InventoryCommand::Execute(vector<string> aArgs, World* aWorld)
{
	aWorld->GetInventory().listInventory();
}