#pragma once

#include <iostream>
#include <string>
#include <list>
#include "Command.h"
#include "World.h"

using namespace std;

class CommandInvoker
{
private:
	list<Command> fCommandQueue;
public:
	void AddCommand(Command aCommand);
	void InvokeCommands(World aWorld, string aArgs[]);
};