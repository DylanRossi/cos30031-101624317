#pragma once

#include <iostream>
#include <string>
#include <vector>
#include "Command.h"

using namespace std;

class World;

class LookCommand : public Command
{
public:
	LookCommand();
	void Execute(vector<string> aArgs, World* aWorld);
};