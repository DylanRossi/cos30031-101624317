#include <iostream>
#include <string>
#include <sstream>
#include "CommandManager.h"

using namespace std;

CommandManager::CommandManager()
{
	fCommands = new multimap<string, Command*>();
	fCommands->insert(pair<string, Command*>("alias", new AliasCommand()));
	fCommands->insert(pair<string, Command*>("look", new LookCommand()));
	fCommands->insert(pair<string, Command*>("inventory", new InventoryCommand()));
	fCommands->insert(pair<string, Command*>("debug", new DebugTreeCommand()));
	fCommands->insert(pair<string, Command*>("help", new HelpCommand()));
}

void CommandManager::Execute(string aInput, World* aWorld)
{
	vector<string> lArgs;
	string lTemp;
	stringstream lSstream(aInput);
	int i = 0;
	while (lSstream.good() && i < 3)
	{
		lSstream >> lTemp;
		lArgs.push_back(lTemp);
		i++;
	}
	if (lArgs.size() == 0)
	{
		return;
	}

	multimap<string, Command*>::iterator it;
	for (it = fCommands->begin(); it != fCommands->end(); it++)
	{
		if (it->first == lArgs[0])
		{
			it->second->Execute(lArgs, aWorld);
		}
	}

}

multimap<string, Command*>* CommandManager::GetCommandList()
{
	return fCommands;
}