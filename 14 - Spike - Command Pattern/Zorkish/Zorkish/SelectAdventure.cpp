#include <iostream>
#include <string>
#include <fstream>
#include "SelectAdventure.h"

using namespace std;

SelectAdventure::SelectAdventure() : Stage()
{
	ifstream lMyFile;
	string lS;
	vector <string> lWorldInfo;

	lMyFile.open("testworld.txt");

	while (getline(lMyFile, lS))
	{
		lWorldInfo.push_back(lS);
	}

	fWorlds.push_back(World(lWorldInfo));
}

SelectAdventure::~SelectAdventure()
{

}

void SelectAdventure::Handle(StageManager* aStageManager)
{
	int lAnswer;

	cout << "Zorkish :: Select Adventure" << endl;
	cout << "--------------------------------------------------------" << endl;
	cout << endl;
	cout << "Choose your adventure:" << endl;
	cout << endl;

	for (int i = 0; i < fWorlds.size(); i++)
	{
		cout << i + 1 << ". " << fWorlds[i].GetName() << endl;
	}

	cout << endl;
	cout << "Select a world:>";
	cin >> lAnswer;

	lAnswer--;
	fWorlds[lAnswer].Handle(aStageManager);
}