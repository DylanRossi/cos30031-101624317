#include <iostream>
#include <string>
#include "MainMenu.h"

using namespace std;

MainMenu::MainMenu() : Stage()
{

}

MainMenu::~MainMenu()
{

}

void MainMenu::Handle(StageManager* aStageManager)
{
	char lAnswer;

	cout << "Zorkish :: Main Menu" << endl;
	cout << "--------------------------------------------------------" << endl;
	cout << endl;
	cout << "Welcome to Zorkish Adventues" << endl;
	cout << endl;
	cout << "1. Select Adventure and Play" << endl;
	cout << "2. Hall of Fame" << endl;
	cout << "3. Help" << endl;
	cout << "4. About" << endl;
	cout << "5. Quit" << endl;
	cout << endl;
	cout << "Select 1-5:> ";
	cin >> lAnswer;

	switch (lAnswer)
	{
	case '1':
		aStageManager->ChangeStage(StageManager::STAGE_SELECTADVENTURE);
		aStageManager->Handle();
		break;
	case '2':
		aStageManager->ChangeStage(StageManager::STAGE_HOF);
		aStageManager->Handle();
		break;
	case '3':
		aStageManager->ChangeStage(StageManager::STAGE_HELP);
		aStageManager->Handle();
		break;
	case '4':
		aStageManager->ChangeStage(StageManager::STAGE_ABOUT);
		aStageManager->Handle();
		break;
	case '5':
		break;
	default:
		cout << "You broke something" << endl;
		break;
	}
}