#pragma once

#include <iostream>
#include <string>
#include "About.h"
#include "Help.h"
#include "HighScore.h"
#include "HOF.h"
#include "MainMenu.h"
#include "SelectAdventure.h"
#include "World.h"

class Stage;

using namespace std;

class StageManager
{
private:
	Stage* fStage;

public:
	enum Stages
	{
		STAGE_MAINMENU,
		STAGE_HELP,
		STAGE_ABOUT,
		STAGE_HIGHSCORE,
		STAGE_SELECTADVENTURE,
		STAGE_HOF,
		STAGE_QUIT
	};

	StageManager();
	~StageManager();

	void Handle();
	void ChangeStage(Stages aStage);
};