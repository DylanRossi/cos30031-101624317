#include <iostream>
#include <string>
#include "LookCommand.h"
#include "World.h"

using namespace std;

LookCommand::LookCommand()
{
}

void LookCommand::Execute(vector<string> aArgs, World* aWorld)
{
	if (aArgs.size() == 1)
	{
		map<int, Item*> lTempItems = aWorld->GetItems();
		vector<int> lTempLocationItems = aWorld->GetCurrentLocation()->GetItemList();
		map<string, int> lTempEdges = aWorld->GetCurrentLocation()->GetEdges();
		map<string, int>::iterator it;
		cout << aWorld->GetCurrentLocation()->GetDescription() << endl;
		cout << "There are paths heading: ";
		for (it = lTempEdges.begin(); it != lTempEdges.end(); it++)
		{
			cout << it->first << " ";
		}
		cout << endl;
		cout << "You can see: " << endl;
		for (int i = 0; i < lTempLocationItems.size(); i++)
		{
			cout << lTempItems.find(lTempLocationItems[i])->second->GetName() << ": " << lTempItems.find(lTempLocationItems[i])->second->GetDescription() << endl;;
		}
	}
	else if (aArgs.size() == 3)
	{
		if (aArgs[1] == "at")
		{
			bool lMatchFound = false;
			map<int, Item*> lTempItems = aWorld->GetItems();
			vector<int> lTempLocationItems = aWorld->GetCurrentLocation()->GetItemList();

			for (int i = 0; i < lTempLocationItems.size(); i++)
			{
				string lTempName = lTempItems.find(lTempLocationItems[i])->second->GetName();
				string lTempSearchItem = aArgs[2];

				for (int j = 0; j < lTempItems.size(); j++)
				{
					lTempName[j] = tolower(lTempName[j]);
				}
				for (int j = 0; j < lTempSearchItem.size(); j++)
				{
					lTempSearchItem[j] = tolower(lTempSearchItem[j]);
				}
				if (lTempName == lTempSearchItem)
				{
					cout << lTempItems.find(lTempLocationItems[i])->second->GetDescription() << endl;
					lMatchFound = true;
				}
			}
			if (!lMatchFound)
			{
				cout << "That item is not available here" << endl;
			}
		}
	}
	else
	{
		cout << "Syntax: look at/in object" << endl;
	}
}