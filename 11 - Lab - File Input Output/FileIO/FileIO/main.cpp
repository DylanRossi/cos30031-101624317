#include <iostream>
#include <fstream>
#include <string>

using namespace std;

struct fileStruct 
{
	char fileChar;
	int fileInt;
	float fileFloat;
} testStruct;

void printStruct()
{
	cout << testStruct.fileChar << " " << testStruct.fileInt << " " << testStruct.fileFloat << endl;
}

int main()
{
	/*testStruct.fileChar = 'd';
	testStruct.fileInt = 20;
	testStruct.fileFloat = 3.14;

	ofstream myFile;
	myFile.open("test1.bin", ios::out | ios::binary);*/

	string line;
	ifstream myFile;
	myFile.open("test1.bin", ios::in | ios::binary);
	
	myFile >> testStruct.fileChar;
	myFile >> testStruct.fileInt;
	myFile >> testStruct.fileFloat;

	/*myFile << testStruct.fileChar << endl;
	myFile << testStruct.fileInt << endl;
	myFile << testStruct.fileFloat << endl;*/

	myFile.close();

	printStruct();

	return 0;
}