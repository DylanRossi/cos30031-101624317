#include <string>
#include "Inventory.h"

using namespace std;

void Inventory::addItem(string item)
{
	inventory.push_back(item);
}

string Inventory::getItem(string itemName)
{
	for (int i = 0; i < inventory.size(); i++)
	{
		if (inventory[i] == itemName)
		{
			string selection = inventory[i];
			inventory.erase(inventory.begin()+i);
			return selection;
		}
	}
	return "No item found";
}

void Inventory::listInventory()
{
	for (int i = 0; i < inventory.size(); i++)
	{
		cout << inventory[i] << endl;
	}
}