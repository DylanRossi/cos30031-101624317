#include <string>
#include "Inventory.h"

using namespace std;

void main()
{
	Inventory testInventory;
	cout << "Add sword and shield to inventory" << endl;
	testInventory.addItem("sword");
	testInventory.addItem("shield");
	cout << "List all items in inventory before removal: " << endl;
	testInventory.listInventory();
	cout << "Get sword: " << endl;
	cout << testInventory.getItem("sword") << endl;
	cout << "Get diamond: " << endl;
	cout << testInventory.getItem("diamond") << endl;
	cout << "List all items in inventory after removal: " << endl;
	testInventory.listInventory();
}