#pragma once

#include <vector>
#include <iostream>

using namespace std;

class Inventory
{
private:
	vector<string> inventory;
public:
	void addItem(string item);
	string getItem(string itemName);
	void listInventory();
};